# Why Data Structures Matter

```puml
@startuml
class Array {
    elements[]
    index
    --
    read
    search
    insert
    delete
}
@enduml
```